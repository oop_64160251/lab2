public class JavaStrings3 {
    public static void main(String[] agrs) {
        String txt = "Hello World";
        System.out.println(txt.toUpperCase()); // Outputs "HELLO WORLD"
        System.out.println(txt.toLowerCase()); // Outputs "hello world"
    }

}
