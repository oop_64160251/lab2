public class JavaTypeCasting1 {
    public static void main(String[] agrs) {
        int myInt = 9;
        double myDouble = myInt; // Automatic casting: int to double

        System.out.println(myInt); // Outputs 9
        System.out.println(myDouble); // Outputs 9.0
        // fixed
    }
}
